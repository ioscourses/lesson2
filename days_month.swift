let amount_of_days = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
let name_of_month = ["January","February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

print("part 1\n")
for i in 0...11 {
    print("\(i + 1)" + ". ",amount_of_days[i])
}

print("\npart 2")
for i in 0...11 {
    print(name_of_month[i], " - ",amount_of_days[i])
}

print("\npart 3")
let days_month = zip(amount_of_days, name_of_month)
for days in days_month{
    print("\(days.1) -  \(days.0)")
}

let reversed_days_month = zip(amount_of_days.reversed(), name_of_month.reversed())
print("\npart 4")
for data in reversed_days_month {
    print("\(data.1) - \(data.0)")
}

let choose_month = 3 
let choose_day = 12
let days_to_date = amount_of_days[..<choose_month].reduce(0, +) + choose_day
print("\nDays to the chosen date: \(days_to_date)")